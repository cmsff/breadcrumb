<?php
$cm = cm::getInstance();
$cm->addEvent("on_load_module", "mod_breadcrumb_cm_on_load_module");

if ($tmp = cm_confCascadeFind(FF_DISK_PATH, "", "mod_breadcrumb.xml"))
	mod_breadcrumb_load_config($tmp);

function mod_breadcrumb_cm_on_load_module($cm, $mod)
{
	$tmp = cm_confCascadeFind(CM_MODULES_ROOT . "/" . $mod . "/conf", "/modules/" . $mod, "mod_breadcrumb.xml");
	if (is_file($tmp))
		mod_breadcrumb_load_config($tmp);
}

function mod_breadcrumb_load_config($file)
{
	$globals = ffGlobals::getInstance("mod_breadcrumb");

	$xml = new SimpleXMLElement("file://" . $file, null, true);

	if (isset($xml->schema) && count($xml->schema->children()))
	{
		if (!isset($globals->schema))
		{
			$globals->schema = new stdClass();
			$globals->schema->root = clone $xml->schema;
			return;
		}

		mod_breadcrumb_iterate_schema($globals->schema->root, $xml->schema);
	}
}

function mod_breadcrumb_iterate_schema (SimpleXMLElement $parent_element, SimpleXMLElement $element)
{
	if (count($attrs = $element->attributes()))
	{
		if (!count($parent_attrs = $parent_element->attributes()))
		{
			foreach ($attrs as $key => $value)
			{
				$parent_element->addAttribute($key, $value);
			}
		}

	}

	if (count($element->children()))
	{
		foreach($element->children() as $key => $value)
		{
			if (!isset($parent_element->$key))
			{
				$tmp = $parent_element->addChild($key);
			}
			
			mod_breadcrumb_iterate_schema($parent_element->$key, $element->$key);
		}
	}
}
