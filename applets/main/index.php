<?php
$globals = ffGlobals::getInstance("mod_breadcrumb");

$path_breadcrumb = $cm->path_info;
$lang_path = "";
if(!isset($globals->lang)) {
    $db = ffDB_Sql::factory();
    $arrPath = explode("/",  trim($path_breadcrumb,"/"));

    $sSQL = "SELECT ff_languages.* 
            FROM ff_languages
            WHERE ff_languages.tiny_code = " . $db->toSql($arrPath[0]);
    $db->query($sSQL);
    if($db->nextRecord()) {
        if($db->getField("status", "Number", true) > 0 && strlen($db->getField("tiny_code", "Text", true))) {
            $globals->lang["ID"] = $db->getField("ID", "Number", true);
            $globals->lang["code"] = $db->getField("code", "Text", true);
            $globals->lang["tiny_code"] = $db->getField("tiny_code", "Text", true);
            $globals->lang["description"] = $db->getField("description", "Text", true);
            
            unset($arrPath[0]);
            
            $path_breadcrumb = "/" . implode("/", $arrPath);
            if(strlen($globals->lang["tiny_code"]))
                $lang_path = "/" . $globals->lang["tiny_code"];
        }
    }        
}



$filename = cm_cascadeFindTemplate("/applets/main/breadcrumb.html", "breadcrumb");
/*
$filename = cm_moduleCascadeFindTemplate(FF_THEME_DISK_PATH, "/modules/breadcrumb/applets/main/breadcrumb.html", $cm->oPage->theme, false);
if ($filename === null)
	$filename = cm_moduleCascadeFindTemplate(CM_MODULES_ROOT . "/breadcrumb/themes", "/applets/main/breadcrumb.html", $cm->oPage->theme);
*/
$tpl = ffTemplate::factory(ffCommon_dirname($filename));
$tpl->load_file("breadcrumb.html", "main");
$tpl->set_var("theme", $cm->oPage->theme);
$tpl->set_var("site_path", $cm->oPage->site_path);

$title = "";
$title_first = "";
$title_last = "";

$path_parts = explode("/", $path_breadcrumb);
if (end($path_parts) == "index")
	unset($path_parts[count($path_parts) - 1]);

if (is_array($path_parts) && count($path_parts))
{
	$path_parts[0] = "root";

	$path = "/";
	$parent = null;
	$into_callback = null;

	$bread_position = $globals->schema;

	foreach($path_parts as $key => $value)
	{
		if (!strlen($value) || $value == "dialog")
			continue;

		$old_title = $title;
		$label = null;
		$link = true;

		if ($key != 0)
			$path = rtrim($path, "/") . "/" . $value;

		if ($into_callback !== null)
		{
			$res = call_user_func_array($into_callback, array($parent, $value));
			if ($res !== null)
			{
				if (isset($res["parent"]))
					$parent = $res["parent"];
				else
					$parent = null;

				if (isset($res["label"]))
					$label = $res["label"];
					
				if (isset($res["title"]))
					$title = $res["title"];
				else
					$title = $title;

				if (isset($res["link"]))
					$link = $res["link"];
			}
			else
			{
				$into_callback = null;
				$parent = null;
			}
		}

		if ($into_callback === null)
		{
			if ($bread_position !== null && isset($bread_position->$value))
			{
				$bread_position = $bread_position->$value;

				if (count($attrs = $bread_position->attributes()))
				{
					if (isset($attrs["exclude"]) && strtolower((string)$attrs["exclude"]) == "true")
						continue;

					if (isset($attrs["label"]))
						$label = (string)$attrs["label"];
					else
						$label = $value;

					if (isset($attrs["title"]))
						$title = (string)$attrs["title"];
					else
						$title = $label;

					if (isset($attrs["callback"]))
						$into_callback = (string)$attrs["callback"];

					if (isset($attrs["nolink"]) && $attrs["nolink"] = "true")
						$link = false;
				}
			}
			else
			{
				$bread_position = null;
				$label = $value;
				$title = $value;
			}
		}

		$title = str_replace("[CM_LOCAL_APP_NAME]", CM_LOCAL_APP_NAME, $title);
		$title = str_replace("[TITLE]", $old_title, $title);

		if ($label != null)
		{
			$tpl->set_var("path", $lang_path . $path . "?" . $cm->oPage->get_globals());
			$tpl->set_var("label", ucwords(preg_replace('/[-]/', ' ', $label)));

			if ($link)
			{
				$tpl->parse("SectLink", false);
				$tpl->set_var("SectNoLink", "");
			}
			else
			{
				$tpl->set_var("SectLink", "");
				$tpl->parse("SectNoLink", false);
			}

			$tpl->parse("SectBreadCrumb", true);
		}
	}
	reset($path_parts);
}

if (MOD_BREAD_SHOW_GUI)
	$tpl->parse("SectGui", false);
else
	$tpl->set_var("SectGui", "");

$out_buffer = $tpl->rpparse("main", false);
//ffErrorHandler::raise("asd", E_USER_ERROR, null, get_defined_vars());
if ($cm->oPage->title != CM_LOCAL_APP_NAME)
	return;

/*if (strlen($title_first) || strlen($title_last) )
{
	$tmp = explode("|", $title);
	$title = "| " . end($tmp);

	if (strlen($title_first))
		$title = $title_first . " " . $title;

	if (strlen($title_last))
		$title = $title_last . " " . $title;
}*/

$cm->oPage->title = $title;
if (isset($cm->oPage->tpl[0]))
	$cm->oPage->tpl[0]->set_var("title", $title);
